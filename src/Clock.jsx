var React = require('react');
var ClockHand = require("./ClockHand");

var Clock = React.createClass({
	getDefaultProps: function(){
		return {
			ampm: false
		};
	},

	getInitialState: function(){
		return {
			timer: null
		};
	},

	componentDidMount: function(){
		window.history.pushState({urlPath:"/clock"},"","/clock");
		var self=this;
		self.state.timer=setInterval(function(){
			!self.refs.second.update() && !self.refs.minute.update() && self.refs.hour.update()
		}, 1000);
	},

	componentWillUnmount: function(){
		clearInterval(this.state.timer);
	},

	render: function(){
		var now = new Date();
		return (
			<div className="timer">
				<h1>CLOCK</h1>
				<ClockHand ref="hour" initVal={this.props.ampm && now.getHours()>11?now.getHours()-12:now.getHours()} max={this.props.ampm?11:23} />
				:
				<ClockHand ref="minute" initVal={now.getMinutes()} />
				:
				<ClockHand ref="second" initVal={now.getSeconds()} />
				<span>{this.props.ampm?(now.getHours()>11?'PM':'AM'):''}</span>
			</div>
		);
	}
});

module.exports = Clock;