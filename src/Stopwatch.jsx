var React = require('react');
var ClockHand = require("./ClockHand");

var Stopwatch = React.createClass({
	STEP: 10,

	getInitialState: function(){
		return {
			mm:0,
			ss:0,
			ms:0,
			timer:null,
			buttonState: "START"
		};
	},

	componentWillUnmount: function(){
		clearInterval(this.state.timer);
	},

	componentDidMount: function(){
		window.history.pushState({urlPath:"/stopwatch"},"","/stopwatch");
	},

	handleClick: function(){
		this.toggleTimer();
		this.setState({
			buttonState: this.state.buttonState=='START'?'STOP':'START'
		});
	},

	toggleTimer: function(){
		var self=this;
		if(self.state.timer){
			clearInterval(self.state.timer);
			self.state.timer=null;
		}else{
			self.state.timer=setInterval(function(){
				!self.refs.millis.update() && !self.refs.second.update() && self.refs.minute.update()
			}, self.STEP);
		}
	},

	resetTimer: function(){
		this.refs.minute.reset();
		this.refs.second.reset();
		this.refs.millis.reset();
	},

	render: function(){
		return (
			<div className="timer">
				<h1>STOPWATCH</h1>
				<ClockHand ref="minute" initVal={this.state.mm} />
				:
				<ClockHand ref="second" initVal={this.state.ss} />
				:
				<ClockHand ref="millis" initVal={this.state.ms} max={1000-this.STEP} step={this.STEP}/>
				

				<div className="controls">
				<button onClick={this.handleClick}>{this.state.buttonState}</button>
				<button onClick={this.resetTimer}>RESET</button>
				</div>
			</div>
		);
	}
});

module.exports = Stopwatch;