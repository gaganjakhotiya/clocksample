var React = require('react');

var ClockHand = React.createClass({
	getDefaultProps: function(){
		return {
			max:59,
			min:0,
			step:1,
			desc:false,
			initVal:0,
			loop:true
		}
	},

	getInitialState: function(){
		return {
			value: this.props.initVal
		}
	},

	update: function(){
		var nextVal;
		if(this.props.desc){
			nextVal=this.state.value==this.props.min?this.props.loop?this.props.max:this.state.value:this.state.value-this.props.step;
		}else{
			nextVal=this.state.value==this.props.max?this.props.loop?this.props.min:this.state.value:this.state.value+this.props.step;
		}
		this.setState({
			value: nextVal
		});
		return nextVal;
	},

	reset: function(){
		this.setState({
			value: this.props.initVal
		});
	},

	setVal: function(val){
		this.setState({
			value:val
		});
	},

	render: function(){
		return (
			<span>{("0"+this.state.value).slice(-2)}</span>
		);
	}
});

module.exports = ClockHand;