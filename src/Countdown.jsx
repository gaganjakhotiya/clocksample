var React = require('react');
var ClockHand = require('./ClockHand');

var Countdown = React.createClass({
	getDefaultProps: function(){
		return {
			hh:0,
			mm:0,
			ss:0,
		};
	},

	getInitialState: function(){
		return {
			hh:this.props.hh,
			mm:this.props.mm,
			ss:this.props.ss,
			buttonState:"START",
			timer: null
		};
	},

	componentDidMount: function(){
		window.history.pushState({urlPath:"/countdown"},"","/countdown");
	},

	componentWillUpdate: function(nextProps, nextState){
		if(this.state.buttonState=="START"){
			this.refs.hour.setVal(parseInt(nextState.hh));
			this.refs.minute.setVal(parseInt(nextState.mm));
			this.refs.second.setVal(parseInt(nextState.ss));
		}
	},

	componentWillUnmount: function(){
		clearInterval(this.state.timer);
	},

	handleClick: function(){
		this.toggleTimer();
		this.setState({
			buttonState: this.state.buttonState=='START'?'STOP':'START'
		});
	},

	handleChange: function(e){
		var val=parseInt(e.target.value);
		this.setState({
			hh:Math.floor(val/3600),
			mm:Math.floor((val%3600)/60),
			ss:val%60
		});
	},

	toggleTimer: function(){
		var self=this;
		if(self.state.timer){
			clearInterval(self.state.timer);
			self.state.timer=null;
		}else{
			self.state.timer=setInterval(function(){
				self.refs.second.update()==59 && self.refs.minute.update()==59 && self.refs.hour.update()
				if(self.refs.second.state.value==0 && self.refs.minute.state.value==0 && self.refs.hour.state.value==0){
					self.handleClick()
				}
			}, 1000);
		}
	},

	render: function(){
		var now = new Date();
		return (
			<div className="timer">
				<h1>TIMER</h1>

				<ClockHand ref="hour" initVal={this.state.hh} max={23} desc={true} loop={false}/>
				:
				<ClockHand ref="minute" initVal={this.state.mm} desc={true} loop={!!this.state.hh}/>
				:
				<ClockHand ref="second" initVal={this.state.ss} desc={true} loop={!!this.state.hh || !!this.state.mm}/>

				
				
				<div className="controls">
				<select onChange={this.handleChange}>
					<option value="0">Select an Option</option>
					<option value="10">10 Seconds</option>
					<option value="60">1 Minute</option>
					<option value="3600">1 Hour</option>
					<option value="4830">1Hr 20Mins 30Secs</option>
				</select>
				<button onClick={this.handleClick}>{this.state.buttonState}</button>
				</div>
			</div>
		);
	}
});

module.exports = Countdown;