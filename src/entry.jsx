var React = require('react');
var ReactDOM = require('react-dom');
var Clock = require('./Clock');
var Stopwatch = require('./Stopwatch');
var Countdown = require('./Countdown');

var App = React.createClass({
	getInitialState: function(){
		return {
			app:0
		};
	},
	handleClick: function(val){
		if(this.state.app!=val){
			this.setState({
				app:val
			});
		}
	},
	render: function(){
		return (
			<div className="app">
				<div className="app-box">
					{this.state.app==0?<Clock ampm={true}/>:this.state.app==1?<Stopwatch/>:<Countdown/>}
				</div>
				<br/>
				<button onClick={this.handleClick.bind(this, 0)}>Clock</button>
				<button onClick={this.handleClick.bind(this, 1)}>Stopwatch</button>
				<button onClick={this.handleClick.bind(this, 2)}>Timer</button>
			</div>
		);
	}
});

ReactDOM.render(<App/>, document.getElementById('content'));